﻿using Microsoft.Win32;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace StudioKit.Utilities
{
	public class Helpers
	{
		private readonly IErrorHandler _errorHandler;

		public Helpers(IErrorHandler errorHandler)
		{
			_errorHandler = errorHandler;
		}

		/// <summary>
		/// Looks up a MIME type for a file by its extension.
		/// </summary>
		public static string ContentTypeForFile(string filename)
		{
			string mime = "application/octetstream";
			var extension = Path.GetExtension(filename);

			if (extension != null)
			{
				string ext = extension.ToLower();
				RegistryKey rk = Registry.ClassesRoot.OpenSubKey(ext);

				if (rk != null && rk.GetValue("Content Type") != null)
					mime = rk.GetValue("Content Type").ToString();
			}

			return mime;
		}

		public static List<string> SplitEmailAddresses(string emails)
		{
			return
				(emails ?? "".Trim()).Split(new[] { "\r\n", "\n", "\r", "\t", ",", ";", " " },
					StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
		}

		public static IQueryable<T> TakeAndSkip<T>(IQueryable<T> query, int take, int skip)
		{
			if (take > 0)
				query = query.Take(take);

			if (skip > 0)
				query = query.Skip(skip);

			return query;
		}

		public static string RelativeToAbsoluteURL(string url, string fullBaseUrl, string virtualPath)
		{
			if (string.IsNullOrEmpty(url))
				return "";

			if (!url.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
			{
				if (!url.StartsWith("/"))
					url = "/" + url;

				if (string.IsNullOrWhiteSpace(virtualPath) == false
					&& fullBaseUrl.IndexOf(virtualPath) != -1
					&& url.IndexOf(virtualPath) != -1)
					fullBaseUrl = fullBaseUrl + url.Replace("/" + virtualPath, "");
				else
					fullBaseUrl = fullBaseUrl + url;

				return fullBaseUrl;
			}

			//Default, if starts with http
			return url;
		}

		public static string DisplayDate(DateTime date)
		{
			double minutes = Math.Floor(new TimeSpan(DateTime.Now.Ticks - date.Ticks).TotalMinutes);
			double hours = Math.Floor(new TimeSpan(DateTime.Now.Ticks - date.Ticks).TotalHours);
			if (minutes < 1)
				return "less than a minute ago";
			if (minutes < 2)
				return "about a minute ago";
			if (minutes < 60)
				return minutes + " minutes ago";
			if (hours < 24)
				return hours + " hour" + (Math.Abs(hours - 1) < double.Epsilon ? "" : "s") + " ago";
			return string.Format("{0:ddd MMM dd yyyy}", date);
		}

		public static MemoryStream Serialize<T>(T obj)
		{
			if (obj == null)
				return null;

			DataContractSerializer s = new DataContractSerializer(typeof(T));
			MemoryStream ms = new MemoryStream();
			s.WriteObject(ms, obj);
			return ms;
		}

		public static T Deserialize<T>(MemoryStream ms)
		{
			if (ms == null)
				return default(T);

			DataContractSerializer s = new DataContractSerializer(typeof(T));
			ms.Position = 0;
			return (T)s.ReadObject(ms);
		}

		public static bool IsAttributeValidationValid(object source, out List<ValidationResult> results)
		{
			ValidationContext context = new ValidationContext(source, null, null);
			results = new List<ValidationResult>();
			bool valid = Validator.TryValidateObject(source, context, results, true);
			return valid;
		}

		public static T ReflectionCopy<T>(object source, T target)
		{
			List<string> changedProperties = null;
			return ReflectionCopy(source, target, out changedProperties);
		}

		public static T ReflectionCopy<T>(object source, T target, out List<string> changedProperties)
		{
			changedProperties = new List<string>();
			var targetType = target.GetType();
			var properties = source.GetType().GetProperties();

			foreach (var property in properties)
			{
				var targetProperty = targetType.GetProperty(property.Name);

				if (targetProperty == null || targetProperty.CanWrite == false ||
					property.PropertyType != targetProperty.PropertyType)
				{
					continue;
				}

				var sourceValue = property.GetValue(source, null);
				var targetValue = targetProperty.GetValue(target, null);

				if (Equals(sourceValue, targetValue) == false)
				{
					changedProperties.Add(targetProperty.Name);
					targetProperty.SetValue(target, sourceValue, null);
				}
			}

			return target;
		}

		/// <summary>
		/// Generates and returns an MD5 hash of a string
		/// </summary>
		public static string MD5String(string originalString)
		{
			//Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] originalBytes = Encoding.Default.GetBytes(originalString);
			byte[] encodedBytes = md5.ComputeHash(originalBytes);

			//Convert encoded bytes back to a 'readable' string
			return BitConverter.ToString(encodedBytes).Replace("-", "").ToLower();
		}

		/// <summary>
		/// Generates and returns an MD5 salted hash of a string in this format: MD5(SALT + string + SALT)
		/// </summary>
		public static string MD5StringSalted(string originalString, string salt)
		{
			return MD5String(salt + originalString + salt);
		}

		/// <summary>
		/// Generates a clean GUID suitable for filenames, URLs, or other sources.
		/// </summary>
		public static string CleanGUID()
		{
			return Guid.NewGuid().ToString().Replace("-", "").ToLower();
		}

		/// <summary>
		/// Creates an object from the collection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="nvc"></param>
		/// <returns></returns>
		public static T DeserializeObject<T>(NameValueCollection nvc) where T : new()
		{
			var obj = new T();

			if (nvc == null)
				return obj;

			foreach (var property in typeof(T).GetProperties())
			{
				var valueAsString = nvc[property.Name];

				if (valueAsString == null)
					continue;

				var converter = TypeDescriptor.GetConverter(property.PropertyType);
				var value = converter.ConvertFrom(valueAsString);
				property.SetValue(obj, value, null);
			}

			return obj;
		}

		/// <summary>
		/// Downloads the image from a URL to the file path.
		/// </summary>
		/// <param name="pictureURL">The URL of the image.</param>
		/// <param name="filePath">The file path to save the image.</param>
		/// <param name="format">The desired format of the image to save.</param>
		/// <returns>True if able to save the image, false otherwise.</returns>
		public bool DownloadImage(string pictureURL, string filePath, ImageFormat format)
		{
			var webRequest = WebRequest.Create(pictureURL);

			try
			{
				using (var webResponse = webRequest.GetResponse())
				{
					using (var responseStream = webResponse.GetResponseStream())
					{
						if (responseStream == null)
							throw new NullReferenceException("responseStream cannot be null");

						using (var image = Image.FromStream(responseStream))
						{
							image.Save(filePath, format);
						}

						GC.Collect();
						GC.WaitForPendingFinalizers();
					}
				}

				return true;
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return false;
			}
		}

		/// <summary>
		/// Converts a DateTime to a UnixTimestamp in seconds (or milliseconds)
		/// </summary>
		public long UnixTimestampFromDateTime(DateTime datetime, bool milliseconds = false)
		{
			int factor = milliseconds ? 10000 : 10000000;
			try
			{
				return (datetime.ToUniversalTime().Ticks - 621355968000000000) / factor;
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return 0;
			}
		}

		/// <summary>
		/// Converts a UnixTimestamp in seconds (or milliseconds) to a DateTime
		/// </summary>
		public DateTime DateTimeFromUnixTimestamp(long timestamp, bool milliseconds = false)
		{
			int factor = milliseconds ? 10000 : 10000000;
			try
			{
				return new DateTime(timestamp * factor + 621355968000000000).ToEasternStandardTime();
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				//Call self with 0 on error (valid, unix epoch)
				return DateTimeFromUnixTimestamp(0);
			}
		}

		/// <summary>
		/// This is what helps us create quality thumbnails. It maintains the ratio of the width, height pixels for the images.
		/// </summary>
		/// <param name="height"></param>
		/// <param name="width"></param>
		/// <param name="maxHeight"></param>
		/// <param name="maxWidth"></param>
		/// <param name="percentage"></param>
		/// <param name="keepAspect"></param>
		/// <param name="stretch"></param>
		/// <returns></returns>
		public static Rectangle ResizeAspect(int height, int width, int maxHeight, int maxWidth, double percentage,
			bool keepAspect, bool stretch)
		{
			//Defaults
			Rectangle rect = new Rectangle(0, 0, width, height);
			double ratio = 0;

			if (percentage > 0) //We are doing a percentage resize
			{
				rect.Height = (int)(height * (percentage / 100));
				rect.Width = (int)(width * (percentage / 100));
			}
			else if (!keepAspect) //Boxed resize
			{
				if (maxWidth > 0 && width > maxWidth)
					rect.Width = maxWidth;
				if (maxHeight > 0 && height > maxHeight)
					rect.Height = maxHeight;

				if (stretch)
				{
					if (maxWidth > 0)
						rect.Width = maxWidth;
					if (maxHeight > 0)
						rect.Height = maxHeight;
				}
			}
			else //We are doing a normal resize
			{
				if (stretch) //Stretch
				{
					if (height > width) //Taller than wide
					{
						if (maxHeight > 0)
						{
							ratio = (double)width / (double)height;
							rect.Height = maxHeight;
							rect.Width = (int)(maxHeight * ratio);
						}
						else if (maxWidth > 0)
						{
							ratio = (double)height / (double)width;
							rect.Width = maxWidth;
							rect.Height = (int)(maxWidth * ratio);
						}
					}
					else //Wider than tall
					{
						if (maxWidth > 0)
						{
							ratio = (double)height / (double)width;
							rect.Width = maxWidth;
							rect.Height = (int)(maxWidth * ratio);
						}
						else if (maxHeight > 0)
						{
							ratio = (double)width / (double)height;
							rect.Height = maxHeight;
							rect.Width = (int)(maxHeight * ratio);
						}
					}
				}
				else //No stretch
				{
					if (height > width) //Taller than wide
					{
						if (maxHeight > 0 && height > maxHeight)
						{
							ratio = (double)width / (double)height;
							rect.Height = maxHeight;
							rect.Width = (int)(maxHeight * ratio);
						}
						else if (maxWidth > 0 && width > maxWidth)
						{
							ratio = (double)height / (double)width;
							rect.Width = maxWidth;
							rect.Height = (int)(maxWidth * ratio);
						}
					}
					else //Wider than tall
					{
						ratio = (double)height / (double)width;
						if (maxWidth > 0 && width > maxWidth)
						{
							ratio = (double)height / (double)width;
							rect.Width = maxWidth;
							rect.Height = (int)(maxWidth * ratio);
						}
						else if (maxHeight > 0 && height > maxHeight)
						{
							ratio = (double)width / (double)height;
							rect.Height = maxHeight;
							rect.Width = (int)(maxHeight * ratio);
						}
					}
				}
			}

			//Final checking... must be at least 1x1
			if (rect.Width == 0)
				rect.Width = 1;
			if (rect.Height == 0)
				rect.Height = 1;

			//And not TOO large...
			if (rect.Width > 8000)
				rect.Width = 8000;
			if (rect.Height > 8000)
				rect.Height = 8000;

			return rect;
		}

		public static DateTime SQLMinDate => new DateTime(1753, 1, 1, 0, 0, 0);

		public static DateTime SQLMaxDate => new DateTime(9999, 12, 31, 23, 59, 59);

		public static WebResponse HttpGet(string uri)
		{
			var webRequest = WebRequest.Create(uri);
			webRequest.Method = "GET";

			// get the response
			return webRequest.GetResponse();
		}

		/// <summary>
		/// Makes an HTTP POST with the specified URL and parameters
		/// </summary>
		/// <param name="uri">The URL to POST to</param>
		/// <param name="parameters">Example: name1=value1&amp;name2=value2</param>
		/// <param name="checkResponse">Reads the response from the request. If this is false then a timeout will not occur.</param>
		/// <param name="timeout">The timeout of the request.</param>
		/// <returns>HTTP Response</returns>
		public WebResponse HttpPost(string uri, string parameters, bool checkResponse = true, int timeout = 30000)
		{
			WebRequest webRequest = WebRequest.Create(uri);
			webRequest.Timeout = timeout;
			webRequest.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
			webRequest.Method = "POST";
			byte[] bytes = Encoding.UTF8.GetBytes(parameters);
			Stream os = null;

			try
			{
				// send the Post
				webRequest.ContentLength = bytes.Length; //Count bytes to send
				os = webRequest.GetRequestStream();
				os.Write(bytes, 0, bytes.Length); //Send it
			}
			finally
			{
				if (os != null)
					os.Close();
			}

			if (checkResponse == false)
				return null;

			try
			{
				// get the response
				WebResponse webResponse = webRequest.GetResponse();

				if (webResponse == null)
					return null;

				return webResponse;
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				throw ex;
			}
		}

		public bool HasUrl(string text)
		{
			try
			{
				var URLs = new List<string>();

				//Split text into "words", then check for URLs in each word
				var words = text.Split(' ', ',', ';');
				foreach (var word in words)
				{
					if (word.Contains("http://") || word.Contains("https://"))
					{
						var matches = Regex.Matches(word, "(http.*?)(?:\\s|\\Z)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
						//Url = UrlMatch.Groups[1].Value;
						URLs.AddRange(from Match match in matches select match.Groups[1].Value);
					}
					else if (word.Contains("www."))
					{
						var matches = Regex.Matches(word, "(www\\..*?)(?:\\s|\\Z)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
						//Url = "http://" + UrlMatch.Groups[1].Value;
						URLs.AddRange(from Match match in matches select "http://" + match.Groups[1].Value);
					}
				}

				//Got some non-empty URLs?
				return URLs.Any(x => !string.IsNullOrWhiteSpace(x));
			}
			catch (Exception ex)
			{
				_errorHandler.CaptureException(ex);
				return false;
			}
		}

		/// <summary>
		/// This will strip all non alpha numeric characters from a vanity name
		/// </summary>
		/// <param name="vanityName">vanity name to be cleaned</param>
		/// <returns>washed vanity name</returns>
		public static string CleanVanityName(string vanityName)
		{
			var regex = new Regex("[^a-zA-Z0-9 -]");
			return regex.Replace(vanityName, "");
		}

		/// <summary>
		/// Finds proposed vanity name for VanityNameEntity
		/// </summary>
		/// <param name="suffix">int of suffix</param>
		/// <param name="proposedVanityName">current proposed vanity name</param>
		/// <returns>proposed vanity name for badge or group</returns>
		public static string ProposedVanityName(int suffix, string proposedVanityName)
		{
			var suffixPosition = proposedVanityName.LastIndexOf('-');
			if (suffixPosition > 0) //if there's already a suffix on it
			{
				var lastIncrement = proposedVanityName.Substring(proposedVanityName.LastIndexOf('-') + 1);
				if (lastIncrement.Equals(suffix.ToString()))
				{
					suffix++;
					proposedVanityName = string.Format("{0}-{1}",
						proposedVanityName.Substring(0, proposedVanityName.LastIndexOf('-')), suffix);
				}
				else
				{
					//suffix doesn't match, go forth and prosper
					suffix++;
					proposedVanityName = string.Format("{0}-{1}", proposedVanityName, suffix);
				}
			}
			else
			{
				//there's no suffix, so add one
				suffix++;
				proposedVanityName = string.Format("{0}-{1}", proposedVanityName, suffix);
			}
			return proposedVanityName;
		}

		private static DateTime? _linkerTimestamp;

		public static DateTime LinkerTimestamp => (DateTime)(_linkerTimestamp ?? (_linkerTimestamp = RetrieveLinkerTimestamp()));

		private static DateTime RetrieveLinkerTimestamp()
		{
			var filePath = Assembly.GetCallingAssembly().Location;
			const int cPeHeaderOffset = 60;
			const int cLinkerTimestampOffset = 8;
			var b = new byte[2048];
			Stream s = null;

			try
			{
				s = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				s.Read(b, 0, 2048);
			}
			finally
			{
				if (s != null)
				{
					s.Close();
				}
			}

			var i = BitConverter.ToInt32(b, cPeHeaderOffset);
			var secondsSince1970 = BitConverter.ToInt32(b, i + cLinkerTimestampOffset);
			var dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			dt = dt.AddSeconds(secondsSince1970);
			dt = dt.ToLocalTime();
			return dt;
		}
	}
}