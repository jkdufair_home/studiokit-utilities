﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Utilities
{
	public class DistributedProcessing
	{
		/// <summary>
		///     This method is used to start a background asynchronous process.  You provide it a parameterless method that is
		///     guaranteed to terminate,
		///     a period for re-invokation, and also some configuration and it will execute that method on a separate thread every
		///     period specified.
		///     If the app is running locally and you want the daemon to run, you'll need to create an environment var with the
		///     namespace-qualified method in order for the task to run
		/// </summary>
		/// <param name="method">The method to call (usually a static method but any instance of the Action class will work)</param>
		/// <param name="period">How often, in milliseconds, to run the action</param>
		/// <param name="environmentName">The name of the current app config environment</param>
		/// <param name="notificationAction">An action used for sending out notifications</param>
		/// <param name="logAction">An action for writing to a log</param>
		/// <param name="tokenSyncFunc">
		///     A function that creates a sync token indicating which host ran the task last. Returns a
		///     boolean if the token was updated
		/// </param>
		/// <example>
		///     DistributedProcessing.StartLoadBalancedDaemon(SomeNamespace.SomeMethod, 1000*60*60*24,
		///     StudioKitConfig.EnvironmentDetail.Name, (errorMessage) => Error.LogError(errorMessage), (logMessage) =>
		///     Log4NetLogger.Logger.Info(logMessage), (methodName, nextPeriod) =>
		///     cskHostSync.UpdateTokenTimestamp(FullMethodName(method), nextPeriod))
		/// </example>
		public static void StartLoadBalancedDaemon(Action method, int period, string environmentName,
			Action<string> notificationAction, Action<string> logAction, Func<string, DateTime, bool> tokenSyncFunc)
		{
			if (environmentName == "Local" && Environment.GetEnvironmentVariable(FullMethodName(method)) == null)
				return;
			// ReSharper disable once ImplicitlyCapturedClosure
			// Closure is intentional
			var task = new Task(() => DaemonLoop(method, period, tokenSyncFunc), TaskCreationOptions.LongRunning);
			var taskForMethod = string.Format("Task for method {0}", FullMethodName(method));
			// ReSharper disable once ImplicitlyCapturedClosure
			// Closure is intentional
			task.ContinueWith(t =>
				{
					notificationAction(t.Exception != null
						? t.Exception.InnerException.Message
						: (string.Format("{0} ended without an exception.  WTF?", taskForMethod)));
					DaemonLoop(method, period, tokenSyncFunc);
					var newTask = new Task(() => DaemonLoop(method, period, tokenSyncFunc),
						TaskCreationOptions.LongRunning);
					newTask.Start();
					logAction(string.Format("{0} task restarted", taskForMethod));
				},
				TaskContinuationOptions.OnlyOnFaulted);
			task.Start();
			logAction(string.Format("{0} started", taskForMethod));
		}

		private static string FullMethodName(Action method)
		{
			return string.Format("{0}.{1}",
				method.Method.DeclaringType == null ? "" : method.Method.DeclaringType.FullName,
				method.Method.Name);
		}

		private static void DaemonLoop(Action method, int period, Func<string, DateTime, bool> shouldRun)
		{
			var nextPeriod = new Random().Next(period) + period / 2;
			while (true)
			{
				var nextRunDate = DateTime.UtcNow.AddMilliseconds(-nextPeriod);
				if (shouldRun(FullMethodName(method), nextRunDate))
					method.Invoke();

				Thread.Sleep(nextPeriod);
				nextPeriod = new Random().Next(period) + period / 2;
			}
			// ReSharper disable once FunctionNeverReturns
			// Yep.  It's a daemon
		}
	}
}