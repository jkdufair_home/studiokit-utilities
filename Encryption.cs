using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Security;
// ReSharper disable InconsistentNaming
// 'AES'

namespace StudioKit.Utilities
{
	//Borrowed from here:
	//http://stackoverflow.com/questions/202011/encrypt-decrypt-string-in-net

	public class Encryption
	{
		//Preconfigured Password Key Derivation Parameters
		public static readonly int SaltBitSize = 128;
		public static readonly int Iterations = 10000;
		private static readonly SecureRandom Random = new SecureRandom();

		/// <summary>
		/// Secure salt generation. Returns salt for inclusion in payload
		/// See https://gist.github.com/jbtule/4336842#file-aesthenhmac-cs and 
		/// https://stackoverflow.com/questions/202011/encrypt-and-decrypt-a-string
		/// TODO: encorporate that entire class here
		/// </summary>
		/// <param name="plaintext">The text to encrypt</param>
		/// <param name="sharedSecret">A password used to generate a key for encryption</param>
		/// <returns>An encrypted string</returns>
		public static (string, string) EncryptStringAES(string plaintext, string sharedSecret)
		{
			var generator = new Pkcs5S2ParametersGenerator();
			//Use Random Salt to minimize pre-generated weak password attacks.
			var salt = new byte[SaltBitSize / 8];
			Random.NextBytes(salt);

			generator.Init(
				PbeParametersGenerator.Pkcs5PasswordToBytes(sharedSecret.ToCharArray()),
				salt,
				Iterations);

			return (Convert.ToBase64String(salt), EncryptStringAES(plaintext, sharedSecret, salt));
		}

		/// <summary>
		/// Encrypt the given string using AES.  The string can be decrypted using
		/// DecryptStringAES().  The sharedSecret parameters must match.
		/// </summary>
		/// <param name="plainText">The text to encrypt.</param>
		/// <param name="sharedSecret">A password used to generate a key for encryption.</param>
		/// <param name="salt">The salt of the application.</param>
		public static string EncryptStringAES(string plainText, string sharedSecret, string salt)
		{
			return EncryptStringAES(plainText, sharedSecret, Encoding.UTF8.GetBytes(salt));
		}

		/// <summary>
		/// Encrypt the given string using AES.  The string can be decrypted using
		/// DecryptStringAES().  The sharedSecret parameters must match.
		/// </summary>
		/// <param name="plainText">The text to encrypt.</param>
		/// <param name="sharedSecret">A password used to generate a key for encryption.</param>
		/// <param name="salt">The salt of the application.</param>
		public static string EncryptStringAES(string plainText, string sharedSecret, byte[] salt)
		{
			if (string.IsNullOrEmpty(plainText))
				throw new ArgumentNullException("plainText");

			string outStr; // Encrypted string to return
			RijndaelManaged aesAlg = null; // RijndaelManaged object used to encrypt the data.

			try
			{
				// generate the key from the shared secret and the salt
				var key = new Rfc2898DeriveBytes(sharedSecret, salt);

				// Create a RijndaelManaged object
				// with the specified key and IV.
				aesAlg = new RijndaelManaged();
				aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
				aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

				// Create a decrytor to perform the stream transform.
				ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

				// Create the streams used for encryption.
				using (var msEncrypt = new MemoryStream())
				{
					using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
					{
						using (var swEncrypt = new StreamWriter(csEncrypt))
						{
							//Write all data to the stream.
							swEncrypt.Write(plainText);
						}
					}
					outStr = Convert.ToBase64String(msEncrypt.ToArray());
				}
			}
			finally
			{
				// Clear the RijndaelManaged object.
				if (aesAlg != null)
					aesAlg.Clear();
			}

			// Return the encrypted bytes from the memory stream.
			return outStr;
		}

		/// <summary>
		/// Decrypt the given string.  Assumes the string was encrypted using
		/// EncryptStringAES(), using an identical sharedSecret.
		/// </summary>
		/// <param name="cipherText">The text to decrypt.</param>
		/// <param name="sharedSecret">A password used to generate a key for decryption.</param>
		/// <param name="salt">The salt of the application.</param>
		public static string DecryptStringAES(string cipherText, string sharedSecret, string salt)
		{
			return DecryptStringAES(cipherText, sharedSecret, Encoding.UTF8.GetBytes(salt));
		}

		/// <summary>
		/// Decrypt the given string.  Assumes the string was encrypted using
		/// EncryptStringAES(), using an identical sharedSecret.
		/// </summary>
		/// <param name="cipherText">The text to decrypt.</param>
		/// <param name="sharedSecret">A password used to generate a key for decryption.</param>
		/// <param name="salt">The salt of the application.</param>
		public static string DecryptStringAES(string cipherText, string sharedSecret, byte[] salt)
		{
			if (string.IsNullOrEmpty(cipherText))
				throw new ArgumentNullException("cipherText");

			// Declare the RijndaelManaged object
			// used to decrypt the data.
			RijndaelManaged aesAlg = null;

			// Declare the string used to hold
			// the decrypted text.
			string plaintext;

			try
			{
				// generate the key from the shared secret and the salt
				var key = new Rfc2898DeriveBytes(sharedSecret, salt);

				// Create a RijndaelManaged object
				// with the specified key and IV.
				aesAlg = new RijndaelManaged();
				aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
				aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

				// Create a decrytor to perform the stream transform.
				ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
				// Create the streams used for decryption.
				byte[] bytes = Convert.FromBase64String(cipherText);
				using (var msDecrypt = new MemoryStream(bytes))
				{
					using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
					{
						using (var srDecrypt = new StreamReader(csDecrypt))

							// Read the decrypted bytes from the decrypting stream
							// and place them in a string.
							plaintext = srDecrypt.ReadToEnd();
					}
				}
			}
			finally
			{
				// Clear the RijndaelManaged object.
				if (aesAlg != null)
					aesAlg.Clear();
			}

			return plaintext;
		}

		public static byte[] HashWithSalt(string plaintext, byte[] salt)
		{
			var plaintextBytes = Encoding.UTF8.GetBytes(plaintext);
			var algorithm = new SHA256Managed();
			var plaintextPlusSalt = new byte[plaintextBytes.Length + salt.Length];
			Buffer.BlockCopy(plaintextBytes, 0, plaintextPlusSalt, 0, plaintextBytes.Length);
			Buffer.BlockCopy(salt, 0, plaintextPlusSalt, plaintextBytes.Length, salt.Length);
			return algorithm.ComputeHash(plaintextPlusSalt);
		}

		public static byte[] CreateSalt(int length)
		{
			var salt = new byte[length];
			var random = new Random();
			random.NextBytes(salt);
			return salt;
		}
	}
}