using System;
using NodaTime;

namespace StudioKit.Utilities.Extensions
{
	public static class DateTimeExtensions
	{
		/// <summary>
		/// Converts the value of the current System.DateTime to Eastern Standard Time.
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		public static DateTime ToEasternStandardTime(this DateTime dateTime)
		{
			return TimeZoneInfo.ConvertTimeFromUtc(dateTime,
				TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
		}

		public static ZonedDateTime FromUtcDateTimeToZone(this DateTime dateTime, string timeZoneId)
		{
			// assume dateTime is from the database, e.g. is UTC
			var localTime = LocalDateTime.FromDateTime(dateTime);
			var zonedTime = localTime.InZoneStrictly(DateTimeZone.Utc);
			// convert to correct timeZone
			var entryTimeZoneId = timeZoneId ?? "America/Indiana/Indianapolis";
			var entryZone = DateTimeZoneProviders.Tzdb[entryTimeZoneId];
			return zonedTime.ToInstant().InZone(entryZone);
		}
	}
}