using System.Drawing;
using System.Drawing.Drawing2D;

namespace StudioKit.Utilities.Extensions
{
	public static class ImageExtensions
	{
		/// <summary>
		/// A basic cropping function that will crop it to the center.
		/// </summary>
		/// <param name="image"></param>
		/// <returns></returns>
		public static Image SquareCrop(this Image image)
		{
			var width = image.Width;
			var height = image.Height;

			if (width == height)
				return image.Crop(0, 0, height, width);

			if (width > height)
			{
				var horizCrop = (width - height) / 2;

				if (horizCrop == 0)
					return image.Crop(0, 0, width, width);

				return image.Crop(0, horizCrop, 0, horizCrop);
			}

			var vertCrop = (height - width) / 2;

			if (vertCrop == 0)
				return image.Crop(0, 0, height, height);

			return image.Crop(vertCrop, 0, vertCrop, 0);
		}

		/// <summary>
		/// A basic cropping function.
		/// </summary>
		/// <param name="image"></param>
		/// <param name="top"></param>
		/// <param name="left"></param>
		/// <param name="bottom"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static Image Crop(this Image image, int top = 0, int left = 0, int bottom = 0, int right = 0)
		{
			var width = image.Width - (right + left);
			width = width <= 0 ? image.Width : width;
			var height = image.Height - (top + bottom);
			height = height <= 0 ? image.Height : height;

			width = width < height ? height : width;
			height = height < width ? width : height;

			var cropRect = new Rectangle(left, top, width, height);
			var croppedBitmap = new Bitmap(width, height);

			using (var g = Graphics.FromImage(croppedBitmap))
			{
				//These lines enforce high quality resizes. Sacrifices some speed, but I prefer image quality.

				g.SmoothingMode = SmoothingMode.AntiAlias;
				g.InterpolationMode = InterpolationMode.HighQualityBicubic;
				g.PixelOffsetMode = PixelOffsetMode.HighQuality;

				//This line does the actual resize.
				g.DrawImage(image, new Rectangle(0, 0, croppedBitmap.Width, croppedBitmap.Height), cropRect, GraphicsUnit.Pixel);
			}

			return croppedBitmap;
		}

		public static Image Resize(this Image image, int size)
		{
			var cropRect = new Rectangle(0, 0, image.Width, image.Height);
			var croppedBitmap = new Bitmap(size, size);

			using (var g = Graphics.FromImage(croppedBitmap))
			{
				//These lines enforce high quality resizes. Sacrifices some speed, but I prefer image quality.
				g.SmoothingMode = SmoothingMode.AntiAlias;
				g.InterpolationMode = InterpolationMode.HighQualityBicubic;
				g.PixelOffsetMode = PixelOffsetMode.HighQuality;

				//This line does the actual resize.
				g.DrawImage(image, new Rectangle(0, 0, croppedBitmap.Width, croppedBitmap.Height), cropRect, GraphicsUnit.Pixel);
			}

			return croppedBitmap;
		}
	}
}