﻿using System;
using System.ComponentModel;

namespace StudioKit.Utilities.Extensions
{
	public static class EnumExtensions
	{
		/// <summary>
		/// Return the string value of the "Description" attribute of an Enum value
		/// </summary>
		/// <param name="value">The Enum value</param>
		/// <returns>The Enum's Description</returns>
		public static string GetDescription(this Enum value)
		{
			var type = value.GetType();
			var name = Enum.GetName(type, value);
			if (name == null) return null;
			var field = type.GetField(name);
			if (field == null) return null;
			var attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
			return attr?.Description;
		}
	}
}