using System;
using System.Text;

namespace StudioKit.Utilities.Extensions
{
	public static class ExceptionExtensions
	{
		public static string GenerateReport(this Exception ex)
		{
			var sb = new StringBuilder();

			int sublevel = 1;
			Exception thisException = ex;

			while (thisException != null)
			{
				sb.Append("----------------------------------------------------------\r\n\r\n");

				sb.Append(thisException.GetType() + " (Sub-Level " + sublevel + "): " + thisException.Message + "\r\n\r\n");
				sb.Append("Trace:\r\n" + thisException.StackTrace + "\r\n");

				thisException = thisException.InnerException;
				sublevel++;
			}

			return sb.ToString();
		}
	}
}